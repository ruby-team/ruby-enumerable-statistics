require 'gem2deb/rake/spectask'

if RbConfig::CONFIG["arch"] =~ /i386/
  task :default do
    puts 'I: skipping tests on i386 as they need SSE2'
  end
else
  Gem2Deb::Rake::RSpecTask.new do |spec|
    spec.pattern = './spec/**/*_spec.rb'
  end
end
